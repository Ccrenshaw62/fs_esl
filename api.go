package esl

// EventName xxx
type EventName string

// APIEvent xxx
type APIEvent  string

// APIEvents xxx
var APIEvents = struct {
	Incoming     APIEvent
	Calling      APIEvent
	Ringing      APIEvent
	Answered     APIEvent
	NoAnswer     APIEvent
	Busy         APIEvent
	Invalid      APIEvent
	Hangup       APIEvent
	DTMF         APIEvent
	Bridged      APIEvent
	Unbridged    APIEvent
	PlayStart    APIEvent
	PlayPause    APIEvent
	PlayResume   APIEvent
	PlayRestart  APIEvent
	PlayStop     APIEvent
	WhisperStart APIEvent
	WhisperStop  APIEvent
	RecordStart  APIEvent
	RecordPause  APIEvent
	RecordResume APIEvent
	RecordStop   APIEvent
}{
	"incoming",
	"calling",
	"ringing",
	"answered",
	"no-answer",
	"busy",
	"invalid",
	"hangup",
	"dtmf",
	"bridged",
	"unbridged",
	"play-start",
	"play-pause",
	"play-resume",
	"play-restart",
	"play-stop",
	"whisper-start",
	"whisper-stop",
	"record-start",
	"record-pause",
	"record-resume",
	"record-stop",
}

// Call xxx
func (c *Channel) Call() {

}

// Answer xxx
func (c *Channel) Answer() error {
	if _, _, err := c.Execute("answer", nil); err != nil {
		return err
	}
	return nil
}

// Hangup xxx
func (c *Channel) Hangup() {

}

// PlayStart xxx
func (c *Channel) PlayStart(file string) {

}

// PlayStop xxx
func (c *Channel) PlayStop() {

}

// DisplaceStart xxx
func (c *Channel) DisplaceStart() {

}

// DisplaceStop xxx
func (c *Channel) DisplaceStop() {

}

// RecordStart xxx
func (c *Channel) RecordStart(file string) {

}

// RecordStop xxx
func (c *Channel) RecordStop() {

}

// WhisperStart xxx
func (c *Channel) WhisperStart(file string) {

}

// WhisperStop xxx
func (c *Channel) WhisperStop() {

}

// SendDTMF xxx
func (c *Channel) SendDTMF(keys string) {

}
