package esl

import (
	"fmt"
	"strconv"
	"sync"
)

// HostChan xxx
type hostChan struct {
	esl      *FSEsl
	host     string
	pass     string
	hostUUID string
	subscribed map[EventName]bool
	Requests chan Request
	Events   chan *Event
	Stop     chan bool
	Done     chan bool
	Error    chan error
}

// Host xxx
type Host struct {
	hostChan
}

// Channel xxx
type Channel struct {
	hostChan
	chanUUID string
}

// Event xxx
type Event struct {
	Name    EventName
	Headers map[string]string
	Content string
}

// Result xxx
type Result struct {
	Succ bool
	Text string
	Data string
	Rqst Request
}

// FSEsl listens to events from the fshosts (one per Freeswitch host) and passes the event to the correct ESL line.
type FSEsl struct {
	mutex    sync.Mutex
	Running  bool
	listen   map[string]*Host		// All hosts providing events through fan-in. Separate from individual Host objects.
	Events   chan *Event  			// All host events (not events from individual host/channel objects)
	Stop     chan bool
	Done     chan bool 				// Reports that this FSEsl has completed.
}

// ESL is the singleton FSEsl object
var ESL *FSEsl

func init() {
	ESL = NewFSEsl()
	return
}

// NewFSEsl creates a new FSEsl. Call its Start method to get things going.
func NewFSEsl() *FSEsl {
	esl := &FSEsl{
		listen:   make(map[string]*Host),
		Events:   make(chan *Event, 100),
		Stop:     make(chan bool),
		Done:     make(chan bool),
	}
	go esl.run()
	return esl
}

// Finish xxx
func (esl *FSEsl) Finish() {
	esl.Stop <-true
}

func (esl *FSEsl) run() {
	esl.Running = true
	for {
		select {
		case <- esl.Stop:
			esl.mutex.Lock()
			defer esl.mutex.Unlock()
			for _, host := range esl.listen {
				host.Stop <-true
				<-host.Done
			}
			esl.Running = false
			esl.Done <-true
			return
		}
	}
}

// NewChannel xxx
func (esl *FSEsl) NewChannel(uuid, addr string, port int, pass string) (*Channel,error) {
	if channel, err := esl.startChannel(addr, port, pass); err == nil {
		channel.chanUUID = uuid
		addFilterReq := NewAddFilter("Unique-ID", channel.chanUUID)
		channel.Requests <- addFilterReq
		addFilterRes := <- addFilterReq.Done()
		if addFilterRes.Succ {
			return channel, nil
		}
		return channel, fmt.Errorf(addFilterRes.Text)
	} else {
		return nil, err
	}
}

// CreateChannel xxx
func (esl *FSEsl) CreateChannel(addr string, port int, pass string) (*Channel,error) {
	if channel, err := esl.startChannel(addr, port, pass); err == nil {
		req := NewCreateChannelID()
		channel.Requests <-req
		select {
		case res := <-req.done:
			if res.Succ {
				channel.chanUUID = res.Text
				addFilterReq := NewAddFilter("Unique-ID", channel.chanUUID)
				channel.Requests <- addFilterReq
				addFilterRes := <- addFilterReq.Done()
				if addFilterRes.Succ {
					return channel, nil
				}
				return channel, fmt.Errorf(addFilterRes.Text)
			} else {
				return channel, fmt.Errorf(res.Text)
			}
		case err := <-req.err:
			return nil, err
		}
	} else {
		return nil, err
	}
}

func (esl *FSEsl) startChannel(addr string, port int, pass string) (*Channel,error) {
	channel := &Channel{}
	channel.initHostChan(esl, addr, port, pass)
	if err := newEslConn(channel.host, pass, channel.Requests, channel.Events, channel.Stop, channel.Done, channel.Error); err != nil {
		return nil, err
	}
	return channel, nil
}

// EventsAdd xxx
func (esl *FSEsl) EventsAdd(addr string, port int, pass string, events []EventName) error {
	var host *Host
	var err error
	var exists bool
	esl.mutex.Lock()

	// If this host is not yet in the esl host list then create a new host (and host connection) and add to esl list.
	// Note that this host list in the esl does not contain any of the hosts that were created externally using NewHost directly.
	if host, exists = esl.listen[addr]; !exists {
		if host, err = esl.NewHost(addr, port, pass); err != nil {
			esl.mutex.Unlock()
			return err
		}
		esl.listen[addr] = host
		esl.mutex.Unlock()
	}
	// We have a connected host in the esl host list. Either was there before, or it's new.
	return host.AddEvents(events)
}

// EventsRem xxx
func (esl *FSEsl) EventsRem(addr string, port int, pass string, events []EventName) error {
	esl.mutex.Lock()

	if host, exists := esl.listen[addr]; !exists {
		// If this host is not yet in the esl host list then we're done.
		esl.mutex.Unlock()
		return nil
	} else {
		// We have a connected host in the esl host list.
		esl.mutex.Unlock()
		return host.RemEvents(events)
	}
}

// NewHost xxx
func (esl *FSEsl) NewHost(addr string, port int, pass string) (*Host,error) {
	host := &Host{}
	host.initHostChan(esl, addr, port, pass)
	if err := newEslConn(host.host, host.pass, host.Requests, host.Events, host.Stop, host.Done, host.Error); err != nil {
		return nil, err
	}
	return host, nil
}

// AddEvents subscribes to the set of events provided
func (h *Host) AddEvents(events []EventName) error {
	// Look at the events we've been given, and put them into a local set if they are not already subscribed to on this host.
	// Use a map(set) to eliminate any accidental duplicates sent in from the caller.
	newEventSet := make(map[EventName]bool)
	for _,eventName := range events {
		if _,found := h.subscribed[eventName]; !found {
			newEventSet[eventName] = true
		}
	}
	// Send the event(s) subscription request to the host.
	addEventReq := NewAddEvents(newEventSet)
	h.Requests <-addEventReq
	addEventRes := <-addEventReq.Done()

	// If the request succeeded, add to the subscribed events list.
	if addEventRes.Succ {
		for eventName := range newEventSet {
			h.subscribed[eventName] = true
		}
		return nil
	}
	return fmt.Errorf(addEventRes.Text)
}

// RemEvents unsubscribes to the set of events provided
func (h *Host) RemEvents(events []EventName) error {
	// Look at the events we've been given, and put them into a local set if they are currently subscribed to on this host.
	// Use a map(set) to eliminate any accidental duplicates sent in from the caller.
	remEventSet := make(map[EventName]bool)
	for _,eventName := range events {
		if _,found := h.subscribed[eventName]; found {
			remEventSet[eventName] = true
		}
	}
	// Send the event(s) subscription request to the host.
	remEventReq := NewRemEvents(remEventSet)
	h.Requests <-remEventReq
	remEventRes := <-remEventReq.Done()

	// If the request succeeded, add to the subscribed events list.
	if remEventRes.Succ {
		for eventName := range remEventSet {
			delete(h.subscribed, eventName)
		}
		return nil
	}
	return fmt.Errorf(remEventRes.Text)
}

func (hc *hostChan) initHostChan(esl *FSEsl, addr string, port int, pass string) {
	hc.esl = esl
	if addr == "" {
		addr = "127.0.0.1"
	}
	if port == 0 {
		port = 8021
	}
	if pass == "" {
		pass = "ClueCon"
	}
	hc.host = fmt.Sprintf("%s:%d", addr, port)
	hc.pass = pass
	hc.Requests = make(chan Request)
	hc.Events = make(chan *Event, 50)
	hc.Stop = make(chan bool)
	hc.Done = make(chan bool)
	hc.Error = make(chan error)
	hc.subscribed = make(map[EventName]bool)
}

// Execute xxx
func (esl *FSEsl) Execute(addr string, port int, pass string, chanUUID, command string, args []string) (string, string, error) {
	var host *Host
	var err error
	var exists bool
	esl.mutex.Lock()

	// If this host is not yet in the esl host list then create a new host (and host connection) and add to esl list.
	// Note that this host list in the esl does not contain any of the hosts that were created externally using NewHost directly.
	if host, exists = esl.listen[addr]; !exists {
		if host, err = esl.NewHost(addr, port, pass); err != nil {
			esl.mutex.Unlock()
			return "", "", err
		}
		esl.listen[addr] = host
		esl.mutex.Unlock()
	}
	// Send the event(s) subscription request to the host.
	executeReq := NewExecuteRequest(chanUUID, command, args)
	host.Requests <-executeReq
	executeRes := <-executeReq.Done()
	executeErr := <-executeReq.Error()
	return executeRes.Text, executeRes.Data, executeErr
}

// Execute will execute the command/args on this channel and return the result.
func (c *Channel) Execute(command string, args []string) (string, string, error) {
	executeReq := NewExecuteRequest(c.chanUUID, command, args)
	c.Requests <-executeReq
	executeRes := <-executeReq.Done()
	executeErr := <-executeReq.Error()
	return executeRes.Text, executeRes.Data, executeErr
}

// EventsAdd xxx
func (c *Channel) EventsAdd(events []EventName) error {
	// Look at the events we've been given, and put them into a local set if they are not already subscribed to on this channel.
	// Use a map(set) to eliminate any accidental duplicates sent in from the caller.
	newEventSet := make(map[EventName]bool)
	for _,eventName := range events {
		if _,found := c.subscribed[eventName]; !found {
			newEventSet[eventName] = true
		}
	}
	// Send the event(s) subscription request to the channel.
	addEventReq := NewAddEvents(newEventSet)
	c.Requests <-addEventReq
	addEventRes := <-addEventReq.Done()

	// If the request succeeded, add to the subscribed events list.
	if addEventRes.Succ {
		for eventName := range newEventSet {
			c.subscribed[eventName] = true
		}
	} else {
		return fmt.Errorf(addEventRes.Text)
	}
	return nil
}

// GetStr xxx
func GetStr(data map[string]string, key, dflt string) string {
	if val, found := data[key]; found {
		return val
	}
	return dflt
}

// GetInt xxx
func GetInt(data map[string]string, key string, dflt int) int {
	if strVal, found := data[key]; found {
		if intVal, err := strconv.Atoi(strVal); err == nil {
			return intVal
		}
	}
	return dflt
}
