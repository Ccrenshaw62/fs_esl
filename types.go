package esl

// EventVars defines the set of event header names
var EventVars = struct {
	EventName string
	FreeswitchIPV4 string
	UniqueID string
	CallDirection string
	CallState string
	CallerContext string
	CallerDestNum string
	CallerCalleeIDNum string
	CallerCalleeIDName string
	CallerCallerIDNum string
	CallerCallerIDName string
	VarFmUserStripped string
	VarToUserStripped string
	EventTime string
	CallerChannelCreatedTime string
	CallerChannelProgressTime string
	CallerChannelMediaTime string
	CallerChannelAnsweredTime string
	CallerChannelTransferTime string
	CallerChannelHangupTime string
	CallerChannelCloseTime string

} {
	"Event-Name",
	"FreeSWITCH-IPv4",
	"Unique-ID",
	"Call-Direction",
	"Call-State", 				// Check this
	"Caller-Context",
	"Caller-Destination-Number",
	"Caller-Callee-ID-Number",
	"Caller-Callee-ID-Name",
	"Caller-Caller-ID-Number",
	"Caller-Caller-ID-Name",
	"variable_sip_from_user_stripped",
	"variable_sip_to_user_stripped",
	"Event-Date-Timestamp",
	"Caller-Channel-Created-Time",
	"Caller-Channel-Progress-Time",
	"Caller-Channel-Progress-Media-Time",
	"Caller-Channel-Answered-Time",
	"Caller-Channel-Transfer-Time",
	"Caller-Channel-Hangup-Time",
	"Caller-Channel-Close-Time", // Check this
}


// EventVals defines some of the fixed set of event header values
var EventVals = struct {
	CallDirectionInbound string
	CallDirectionOutbound string
} {
	"INBOUND",
	"OUTBOUND",
}

// EventNames lists all the values for the Event-Name event variable
var EventNames = struct {
	ChannelCreate EventName
	ChannelDestroy EventName
	RecordStart EventName
	RecordStop EventName
} {
	"CHANNEL_CREATE",
	"CHANNEL_DESTROY",
	"RECORD_START",
	"RECORD_STOP",
}
