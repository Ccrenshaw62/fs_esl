package esl

import (
	"fmt"
	"time"
	"strings"
	"net"
	"bufio"
	"bytes"
	"strconv"
)

type eslconn struct {
	host     string
	pass     string
	requests chan Request
	events   chan *Event
	stop     chan bool
	done     chan bool
	err      chan error

}
type reply struct {
	text string
	data string
	succ bool
}

func newEslConn(host, pass string, requests chan Request, events chan *Event, stop, done chan bool, err chan error) error {
	e := &eslconn{host: host, pass: pass, requests: requests, events: events, stop: stop, done: done, err: err}
	conn := make(chan bool)
	go e.run(conn)
	select {
	case <-conn:
		return nil
	case err :=<-e.err:
		return err
	}
}

func (e *eslconn) run(conn chan bool) {
	initConn := conn
	var retryReq Request
	for {
		retry := make(chan Request)
		reset := make(chan bool)
		done  := make(chan bool)
		err   := make(chan error)

		e.runEslConn(retryReq, retry, reset, done, initConn, err)

		initConn = nil
		retryReq = nil

		select {
		case retryReq =<-retry:
			continue
		case <-reset:
			continue
		case <-done:
			e.done <-true
			return
		case eslErr :=<-err:
			e.err <-eslErr
			return
		}
	}
}

func (e *eslconn) runEslConn(retryRequest Request, retry chan Request, reset, done, conn chan bool, err chan error) {
	var tcp *net.TCPConn

	if tcpConn, tcpErr := newTCPConn(e.host); tcpErr != nil {
		err <-tcpErr
		return
	} else {
		tcp = tcpConn
	}

	rdrEvents   := make(chan *Event, 100)
	rdrReplies  := make(chan *reply)
	rdrSendPass := make(chan bool)
	rdrClose    := make(chan string)
	rdrErrors   := make(chan error)
	authTimeout := time.After(30 * time.Second)

	go runReader(tcp, rdrEvents, rdrReplies, rdrSendPass, rdrClose, rdrErrors)

	reqs := e.requests
	auth := rdrReplies	// Non-nil at first. Once authenticated, this channel is nil
	repl := rdrReplies

	reqs = nil			// Can't take commands from parent until we are authenticated
	repl = nil			// Once authenticated, this channel is set to messages

	stopping  := false
	resetting := false

	var request Request
	var reqTimeout <-chan time.Time
	var reqStartTime time.Time

	for {
		select {
		case <-authTimeout:
			err <-fmt.Errorf("auth-timeout")
			tcp.Close()

		case <-rdrSendPass:
			// Step one - need to be asked for the password - other channels are mostly nil right now, until we are authenticated (connected)
			sendAuth(tcp, e.pass)

		case reply :=<-auth:
			authTimeout = nil
			// Step two - auth is the rderReplies channel - non-nil right now because we need the auth response before anything else.
			if reply.succ {
				conn <-true
				auth = nil			// Success - set auth to nil - Now we read from rdrReplies channel on repl instead of auth (actually same channel).
				repl = rdrReplies	// Read replies from repl instead of auth (same underlying channel).
				reqs = e.requests	// Can now start taking in requests from parent.

				if retryRequest != nil {
					reqs <-retryRequest
					retryRequest = nil
				}
			} else {
				err <-fmt.Errorf(reply.text)
				stopping = true		// So when we get an error from the reader, we know it's a good error.
				tcp.Close()			// Will trigger an error in the reader, and the reader will quit.
			}

		// commands and stop are read in from "parent" (we must be authenticated otherwise this channel would be nil)
		case request = <-reqs:
			// Read commands sent down to us. If we're already in the middle of a command, this will be a nil channel and not readable.
			reqs = nil				// Can only read one command at a time.
			if request.Timeout() > 0 {
				reqStartTime = time.Now() // need this in case we need to restart the request; allows us to adjust the remaining timeout.
				reqTimeout   = time.After(request.Timeout() * time.Second)
			}
			sendAPI(tcp, request)

		case <-reqTimeout:
			resetting = true
			request = nil
			reqStartTime = time.Time{}
			tcp.Close()

		case reply :=<-repl:
			reqs = e.requests		// Request is done - now can read next request from parent.
			request.Done() <-newResult(request, reply.succ, reply.text, reply.data)
			if !reply.succ {
				request.Error() <- fmt.Errorf(reply.text)
			}
			request = nil
			reqStartTime = time.Time{}

		case <-e.stop:
			// If we're told to stop, signal this to the tcp reader by shutting down its connection.
			reqs = nil				// Can't read any more commands from parent
			stopping = true
			request = nil
			tcp.Close()
			
		// These are read from child socket reader
		case event :=<-rdrEvents:
			e.events <-event			// Easy. Just pass straight up.

		case reason :=<-rdrClose:
			tcp.Close()
			stopping = true
			request = nil
			err <-fmt.Errorf(reason)

		case <-rdrErrors:		// Only occurs on readline error on the socket. Retry the connection with a new eslconn, go from there.
			tcp.Close()			// May not need. Just be sure.
			if stopping {		// If we're stopping the eslconn (a command timeout would not stop it, only reset it)
				done <-true
			} else if resetting {
				reset <-true
			} else {			// Got an unexpected TCP error from below (maybe a network burp?). Try to reset.
				if request != nil && (reqStartTime != time.Time{}) && (request.Timeout()-time.Since(reqStartTime)) > 0 {
					// If a request was active, let's try to resend it by passing it up so that it comes back in the next eslconn.
					// Decrement the remaining timeout. This request could run out of time if network keeps dropping.
					// If so, eventually return a timeout error to the caller, so that the caller can decide whether to try again.
					request.SetTimeout(request.Timeout()-time.Since(reqStartTime))
					retry <-request
				} else {
					reset <-true
				}
			}
			return
		}
	}
}

func runReader(conn *net.TCPConn, events chan *Event, replies chan *reply, sendPass chan bool, close chan string, errs chan error) {
	lr := bufio.NewReader(conn)

	for {
		headers := make(map[string]string)
		content := make([]byte, 0)
		buffer  := bytes.Buffer{}
		started := false
		length  := 0
		
		for {
			if line, _, err := lr.ReadLine(); err == nil {
				lineStr := string(line)

				// Identify blank lines. They're important.
				if len(lineStr) == 0 {
					if !started {
						// Got blank line, but haven't seen any text yet, this is a garbage blank line. Skip it.
						continue
					} else if length == 0 {
						// If we were in the msg, now a blank line, and no content-length to read, message completed.
						break
					} else {
						// Blank line, had content, and content-length > 0. Time to read the data!
						content = make([]byte, length)
						for {
							if bytesRead, err := lr.Read(content); err != nil {
								break
							} else {
								buffer.Write(content[:bytesRead])
								length -= bytesRead
								if length <= 0 {
									break
								}
								content = make([]byte, length)
							}
						}
						// If we get here, we're done with this message. Break out of this inner loop that reads this message, go below to process.
						break
					}
				} else {
					// We have a line of text. Make sure this flag is set. Then process the line. These must be headers, not content.
					started = true
					// Must (should) be header name/value pairs.
					hdrNameVal := strings.SplitN(lineStr, ":", 2)
					if len(hdrNameVal) == 2 {
						// Only process lines that look like valid headers. Might have to revisit this!
						hdr := strings.TrimSpace(hdrNameVal[0])
						val := strings.TrimSpace(hdrNameVal[1])
						// This header is special. We need it so we can read the upcoming content. Without the length, don't know how to read it.
						if hdr == "Content-Length" {
							if length, err = strconv.Atoi(val); err != nil {
								length = 0
							}
						}
						headers[hdr] = val
					}
				}
			} else {
				errs <-err
				return
			}
		}

		// When we fall out to here, we have the various fields filled in that make up the message we received.
		// Now figure out what it is so we can send it back correctly.

		if ctype, found := headers["Content-Type"]; !found {
			continue
		} else {
			// We're being asked for our password - happens at the start. Send back a sendPass trigger to our handler. 
			// Our handler will send down the password, and we will then receive the response in a "command-reply". 
			// We'll send that response to our handler, and our handler will decide if/when we are connected.
			if ctype == "auth/request" {
				sendPass <-true

			// "command/reply" only has header "Reply-Text" - no "Content-Length" or content following
			} else if ctype == "command/reply" {
				replyMsg := reply{}
				if replyText, found := headers["Reply-Text"]; found {
					replyMsg.text, replyMsg.succ = parseStatusLine(replyText)
				}
				replies <-&replyMsg

			} else if ctype == "api/response" {
				replyMsg := reply{}
				if replyText, found := headers["Reply-Text"]; found {
					replyMsg.text, replyMsg.succ = parseStatusLine(replyText)
					if len(content) > 0 {
						replyMsg.data = string(content)
					}
				}
				replies <-&replyMsg

			// With a plain text event, all name/value pairs are in the headers map already.
			} else if ctype == "text/event-plain" {
				eventMsg := Event{}
				eventMsg.Headers = headers
				eventMsg.Content = string(content)
				scanner := bufio.NewScanner(bytes.NewReader(content))
				for scanner.Scan() {
					lineStr := scanner.Text()
					hdrNameVal := strings.SplitN(lineStr, ":", 2)
					if len(hdrNameVal) == 2 {
						hdr := strings.TrimSpace(hdrNameVal[0])
						val := strings.TrimSpace(hdrNameVal[1])
						eventMsg.Headers[hdr] = val
					}
				}
				events <-&eventMsg

			// With a json event, content type and length are in headers, name/value pairs are in the content.
			} else if ctype == "text/json" || ctype == "application/json" {
				// Parse out json, add to headers
				eventMsg := Event{}
				eventMsg.Headers = headers
				eventMsg.Content = string(content)
				events <-&eventMsg

			// With an xml event, content type and length are in headers, name/value pairs are in the content.
			} else if ctype == "text/xml" {
				// Parse out xml, add to headers
				eventMsg := Event{}
				eventMsg.Headers = headers
				eventMsg.Content = string(content)
				events <-&eventMsg

			} else if ctype == "text/disconnect-notice" {
				// The channel has closed. We're done here. The receiver of the done event will take care of the socket.
				close <-"remote-close"
				return

			} else if ctype == "text/rude-rejection" {
				// On attempting to authenticate, the server can reject us because of our IP address. Since a new pass won't help, send back special failure.
				close <-"remote-reject"
				return

			} else if ctype == "log/data" {
				// For now, do nothing with log data. Hopefully we didn't register for it.
			}
		}
	}
}

func parseStatusLine(line string) (text string, succ bool) {
	tokens := strings.SplitN(line, " ", 2)
	if len(tokens) > 0 {
		if tokens[0] == "+OK" {
			succ = true
		} else if tokens[0] == "-ERR" {
			succ = false
		}
		if len(tokens) > 1 {
			text = tokens[1]
		}
	}
	return
}

func newTCPConn(hostPort string) (*net.TCPConn, error) {
	if tcpAddr, err := net.ResolveTCPAddr("tcp", hostPort); err == nil {
		if tcpConn, err := net.DialTCP("tcp", nil, tcpAddr); err != nil {
			return nil, err
		} else {
			return tcpConn, nil
		}
	} else {
		return nil, err
	}
}
func newResult(rqst Request, succ bool, text, data string) *Result {
	result := &Result{}
	result.Rqst = rqst
	result.Succ = succ
	result.Text = text
	result.Data = data
	return result
}

func sendAPI(tcpConn *net.TCPConn, rqst Request) error {
	commandLine := fmt.Sprintf("%s\r\n\r\n", rqst)
	fmt.Printf("Sending API %s\n", commandLine)
	if _, err := fmt.Fprintf(tcpConn, commandLine); err != nil {
		return err
	}
	return nil
}
func sendAuth(tcpConn *net.TCPConn, pass string) error {
	commandLine := fmt.Sprintf("auth %s\r\n\r\n", pass)
	fmt.Printf("Sending auth %s\n", commandLine)
	if _, err := fmt.Fprintf(tcpConn, commandLine); err != nil {
		return err
	}
	return nil
}