package esl

import (
	"strings"
	"fmt"
	"time"
)

// Request xxx
type Request interface {
	String() string
	Timeout() time.Duration
	Done() chan *Result
	Error() chan error
	SetTimeout(timeoutSecs time.Duration)
	SetDone(chan *Result)
	SetError(chan error)
}

// RequestFields xxx
type RequestFields struct {
	Request
	timeoutSecs time.Duration
	done chan *Result
	err  chan error
}
func (rf *RequestFields) initRequestFields() {
	rf.done = make(chan *Result)
	rf.err  = make(chan error)
}
// Done xxx
func (rf *RequestFields) Done() chan *Result {
	return rf.done
}
// Timeout xxx
func (rf *RequestFields) Timeout() time.Duration {
	return rf.timeoutSecs
}
// Error xxx
func (rf *RequestFields) Error() chan error {
	return rf.err
}
// SetDone xxx
func (rf *RequestFields) SetDone(newDone chan *Result) {
	rf.done = newDone
}
// SetError xxx
func (rf *RequestFields) SetError(newErr chan error) {
	rf.err = newErr
}
// SetTimeout xxx
func (rf *RequestFields) SetTimeout(timeoutSecs time.Duration) {
	rf.timeoutSecs = timeoutSecs
}

// CreateChannelID xxx
type CreateChannelID struct {
	RequestFields
}

// AddEvents xxx
type AddEvents struct {
	RequestFields
	Events []EventName
}

// RemEvents xxx
type RemEvents struct {
	RequestFields
	Events []EventName
}

// AddFilter xxx
type AddFilter struct {
	RequestFields
	Name string
	Value string
}

// RemFilter xxx
type RemFilter struct {
	RequestFields
	Name string
	Value string
}

// ExecuteRequest xxx
type ExecuteRequest struct {
	RequestFields
	chUUID string
	command string
	arguments []string
}

// Call xxx
type Call struct {
	RequestFields

}

// Hangup xxx
type Hangup struct {
	RequestFields

}

// NewCreateChannelID xxx
func NewCreateChannelID() *CreateChannelID {
	req := &CreateChannelID{}
	req.initRequestFields()
	return req
}

// NewAddEvents xxx
func NewAddEvents(events map[EventName]bool) *AddEvents {
	req := &AddEvents{}
	req.initRequestFields()
	if events != nil && len(events) > 0 {
		for event := range events {
			req.Events = append(req.Events, event)
		}
	} else {
		req.Events = make([]EventName, 10)
	}
	return req
}

// NewRemEvents xxx
func NewRemEvents(events map[EventName]bool) *RemEvents {
	req := &RemEvents{}
	req.initRequestFields()
	if events != nil && len(events) > 0 {
		for event := range events {
			req.Events = append(req.Events, event)
		}
	} else {
		req.Events = make([]EventName, 10)
	}
	return req
}

// NewAddFilter xxx
func NewAddFilter(name, value string) *AddFilter {
	req := &AddFilter{Name: name, Value: value}
	req.initRequestFields()
	return req
}

// NewRemFilter xxx
func NewRemFilter(name, value string) *RemFilter {
	req := &RemFilter{Name: name, Value: value}
	req.initRequestFields()
	return req
}

// NewExecuteRequest xxx
func NewExecuteRequest(chUUID, command string, arguments []string) *ExecuteRequest {
	req := &ExecuteRequest{chUUID: chUUID, command: command, arguments: arguments}
	req.initRequestFields()
	return req
}

// NewCall xxx - caller can optionally set their own Done and/or Error channels to provide fan-in (should these funcs be on Channel and/or Host?)
func NewCall() *Call {
	req := &Call{}
	req.initRequestFields()
	return req
}

func (e *ExecuteRequest) String() string {
	return fmt.Sprintf("sendmsg %s\ncall-command: execute\nexecute-app-name: %s\nexecute_app-arg: %s\n\n",
		e.chUUID, e.command, strings.Join(e.arguments, " "))
}
func (e *AddEvents) String() string {
	events := make([]string, 0, len(e.Events))
	for _, en := range e.Events {
		events = append(events, string(en))
	}
	return fmt.Sprintf("event plain %s", strings.Join(events, " "))
}

func (e *RemEvents) String() string {
	events := make([]string, 0, len(e.Events))
	for _, en := range e.Events {
		events = append(events, string(en))
	}
	return fmt.Sprintf("event remove %s", strings.Join(events, " "))
}

func (e *AddFilter) String() string {
	return fmt.Sprintf("filter %s %s", e.Name, e.Value)
}

func (e *RemFilter) String() string {
	return fmt.Sprintf("filter delete %s %s", e.Name, e.Value)
}

func (r *CreateChannelID) String() string {
	return ""
}

